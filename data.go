package gocmdi

const (
	// context types
	cWord = "Word"
	cFlag = "Flag"
	cPath = "Path"
	cNone = "None"
)

var tokenMap = map[string]string{
	cWord: "C",
	cFlag: "F",
	cPath: "P",
	cNone: "N",
}

const signaturesNumber = 2

var signatures = [signaturesNumber]string{
	"CP",
	"CFP",
}

var commandsLastIdx = len(commands) - 1
var commands = []string{
	"apt",
	"apt-get",
	"aptitude",
	"awk",
	"base32",
	"base64",
	"bash",
	"cat",
	"cd",
	"chmod",
	"chown",
	"chpasswd",
	"chroot",
	"cp",
	"curl",
	"cut",
	"dig",
	"dir",
	"dirname",
	"dirs",
	"dmesg",
	"dpkg",
	"echo",
	"egrep",
	"env",
	"ethtool",
	"eval",
	"exec",
	"expr",
	"fdformat",
	"fdisk",
	"ftp",
	"fuser",
	"gawk",
	"getfacl",
	"grep",
	"groupadd",
	"groupdel",
	"groupmod",
	"gzip",
	"hostname",
	"ifconfig",
	"ifdown",
	"ifup",
	"kill",
	"killall",
	"ln",
	"ls",
	"mkdir",
	"mount",
	"mtools",
	"mtr",
	"mv",
	"mmv",
	"nc",
	"netstat",
	"nft",
	"nl",
	"nohup",
	"nslookup",
	"passwd",
	"ping",
	"pgrep",
	"pkill",
	"popd",
	"ps",
	"pushd",
	"pwd",
	"rar",
	"rcp",
	"rm",
	"rmdir",
	"scp",
	"sed",
	"sftp",
	"ssh",
	"stat",
	"strace",
	"su",
	"sudo",
	"sum",
	"tar",
	"tee",
	"traceroute",
	"ulimit",
	"umask",
	"umount",
	"unalias",
	"uname",
	"unexpand",
	"uniq",
	"units",
	"unrar",
	"useradd",
	"userdel",
	"usermod",
	"wget",
}

type TestCase struct {
	Value       string
	Fingerprint string
	Legit       bool
}

var TestCases = []TestCase{
	{"hello, how are you doing", "", true},
	{"& cat - cat = 0", "", true},
	{"& cat-cat ?", "", true},
	{"ok; ok; ok", "", true},
	{"& catalog = /some/path", "", true},
	{"; catalog=/some/path", "", true},
	{"<script>  cat/etc/passwd", "", true},

	{"; cat /et?/pas*wd", ";CP", false},
	{"; cat /et[a-z]/pas*w?", ";CP", false},

	{"| rm -r /", "|CFP", false},
	{"|| rm -r /", "|CFP", false},

	{"& rm -r /", "&CFP", false},
	{"&& rm -r /", "&CFP", false},

	{"; rm -r /", ";CFP", false},
}
