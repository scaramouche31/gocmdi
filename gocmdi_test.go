package gocmdi

import (
	"fmt"
	"testing"
)

func TestCMDI(t *testing.T) {
	for _, tc := range TestCases {
		t.Run(fmt.Sprintf("legit=%t", tc.Legit), func(t *testing.T) {
			fingerprint, found := IsCMDI(tc.Value)

			if found == tc.Legit {
				t.Errorf("got %t for `%s`", found, tc.Value)
			}

			if fingerprint != tc.Fingerprint {
				t.Errorf("got %s, wanted %s", fingerprint, tc.Fingerprint)
			}
		})
	}
}

func BenchmarkCMDI(b *testing.B) {
	testCases := []TestCase{
		{"hello, how are you doing", "", true},
		{"ok; ok; ok", "", true},
		{"; rm -r /", "", false},
	}

	for _, tc := range testCases {
		b.Run(fmt.Sprintf("legit=%t", tc.Legit), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				IsCMDI(tc.Value)
			}
		})
	}
}
