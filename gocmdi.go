package gocmdi

import (
	"strings"

	"mvdan.cc/sh/v3/syntax"
)

func IsCMDI(value string) (string, bool) {
	if len(value) < 9 {
		return "", false
	}

	fingerprint, found := isCMDI(value)
	if found {
		idx := strings.Index(value, " ")
		if idx == -1 { // extra OOB check, though its not really possible
			return "", false
		}

		r := strings.NewReader(value[idx:])
		// valid shell code
		if _, err := syntax.NewParser().Parse(r, ""); err == nil {
			return fingerprint, found
		}
	}
	return "", false
}

func isCMDI(value string) (string, bool) {
	// look for starting char
	l := len(value)
	lastIndex := l - 1
	idx, startingChar, found := func() (int, string, bool) {
		for i := 0; i < l; i++ {
			startingChar, found := isStartingChar(value[i])
			if found && i != lastIndex {
				return i + 1, startingChar, true
			} else if found && i == lastIndex {
				break
			}
		}
		return 0, "", false
	}()

	if !found || idx == lastIndex {
		return "", false
	}

	// init state
	s := state{
		value:         value,
		idx:           idx,
		lastIndex:     lastIndex,
		fingerprint:   startingChar,
		tokensChecked: true,
		context:       cNone,
	}
	// start context-driven parsing
	for !s.parsed {
		c := s.value[s.idx]

		switch s.context {
		case cNone:
			if isLetter(c) {
				s.setContext(cWord)
			} else if c == '/' {
				s.setContext(cPath)
			} else if c == '-' {
				s.setContext(cFlag)
			}
		default:
			if c == ' ' {
				s.endContext()
			}
		}
		s.incr()
	}
	s.endContext()

	if len(s.fingerprint) > 1 {
		return s.fingerprint, true
	}
	return "", false
}
