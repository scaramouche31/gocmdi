package gocmdi

type state struct {
	parsed bool

	value     string
	idx       int
	lastIndex int

	fingerprint   string
	tokens        string
	tokensChecked bool

	context      string
	contextStart int
}

func (s *state) setContext(context string) {
	s.context = context
	s.contextStart = s.idx
}

func (s *state) incr() {
	if s.idx < s.lastIndex {
		s.idx++
	} else if s.idx == s.lastIndex {
		s.parsed = true
	}
}

func (s *state) endContext() {
	pattern := func() string {
		if s.idx != s.lastIndex {
			return (s.value[s.contextStart:s.idx])
		} else {
			return (s.value[s.contextStart:])
		}
	}()

	switch s.context {
	case cNone:
		return
	case cPath:
		s.tokens += tokenMap[cPath]
		s.tokensChecked = false
	case cFlag:
		if len(pattern) > 1 {
			s.tokens += tokenMap[cFlag]
			s.tokensChecked = false
		}
	case cWord:
		token, found := checkWord(pattern)
		if found {
			s.tokensChecked = false
		}
		s.tokens += token
	}

	if !s.tokensChecked {
		if fingerprint, found := checkTokens(s.tokens); found {
			s.fingerprint += fingerprint
			s.parsed = true
		}
	}

	s.context = cNone
}
