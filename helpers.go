package gocmdi

func isLetter(c byte) bool {
	if (c >= 65 && c <= 90) || (c >= 97 && c <= 122) {
		return true
	}
	return false
}

func isStartingChar(c byte) (string, bool) {
	if c == ';' || c == '&' || c == '|' {
		return string(c), true
	}
	return "", false
}

func checkWord(word string) (string, bool) {
	leftBound := 0
	rightBound := commandsLastIdx
	for rightBound > leftBound {
		i := (leftBound + rightBound) / 2
		c := commands[i]
		if word == c {
			return tokenMap[cWord], true
		} else if word > c {
			leftBound = i + 1
		} else if word < c {
			rightBound = i
		}
	}
	return tokenMap[cNone], false
}

func checkTokens(tokens string) (string, bool) {
	l := len(tokens)
	for i := 0; i < signaturesNumber; i++ {
		signature := signatures[i]
		signatureLen := len(signature)

		if l >= signatureLen {
			if tokens[l-signatureLen:] == signature {
				return signature, true
			}
		}
	}
	return "", false
}
